let mongoose = require("mongoose");

// Employee Schema
const Categories = mongoose.model("Categories", {
  image: {
    type: String,
    required: true,
  },
  catName: {
    type: String,
    required: true,
  },
  carDesc: {
    type: String,
    required: true,
  }
});

module.exports = {Categories};

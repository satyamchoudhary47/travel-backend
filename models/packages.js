let mongoose = require('mongoose');



// Employee Schema
const Package = mongoose.model('Package', {
    location: {
        type: String,
        required:true
    }, 
    title: {
        type:String,
        required:true
    },
    rating: {
        type:String,
        required:true
    },
    reviewNo: {
        type: String,
        required:true
    }, 
    ogPrice: {
        type:String,
        required:true
    },
    strikePrice: {
        type:String,
        required:true
    },
    placeImg: {
        type: String,
        required:true
    }, 
    iconImg: {
        type:String,
        required:true
    },
    catId: {
        type:String,
        required:true
    }
});



module.exports = {Package}
const express = require('express');
const router = express.Router();
const ObjectId = require('mongoose').Types.ObjectId;    

const { Employee } = require('../models/employee');
const { Register } = require('../models/register');
const { Categories } = require('../models/categories');
const { Package } = require('../models/packages');


// Get All Employees
router.get('/api/employees', (req, res) => {
    Employee.find({}, (err, data) => {
        if(!err) {
            res.send(data);
        } else {
            console.log(err);
        }
    });
});


// Get Single Employee (First Way)

router.get('/api/employee/:id', (req, res) => {
    Employee.findById(req.params.id, (err, data) => {
        if(!err) {
            res.send(data);
        } else {
           console.log(err);
        }
    });
});


// Get Single Employee (2nd Way)

// router.get('/api/employee/:id', (req, res) => {
//     if(!ObjectId.isValid(req.params.id))
//     return res.status(400).send(`No record With Given ID : ${req.params.id}`);

//     Employee.findById(req.params.id, (err, data) => {
//         if(!err) {
//             res.send(data);
//         } else {
//            console.log(err);
//         }
//     });
// });


// Save Employee
router.post('/api/employee/add', (req, res) => {
    const emp = new Employee({
        name: req.body.name,
        email: req.body.email,
        salary: req.body.salary
    });
    emp.save((err, data) => {
        if(!err) {
            // res.send(data);
            res.status(200).json({code: 200, message: 'Employee Added Successfully', addEmployee: data})
        } else {
           console.log(err);
        }
    });
});




// Update Employee

router.put('/api/employee/update/:id', (req, res) => {


    const emp = {
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    };
    Employee.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }, (err, data) => {
        if(!err) {
            res.status(200).json({code: 200, message: 'Employee Updated Successfully', updateEmployee: data})
        } else {
            console.log(err);
        }
    });
});





// Delete Employee
router.delete('/api/employee/:id', (req, res) => {

    Employee.findByIdAndRemove(req.params.id, (err, data) => {
        if(!err) {
            // res.send(data);
            res.status(200).json({code: 200, message: 'Employee deleted', deleteEmployee: data})
        } else {
            console.log(err);
        }
    });
});


// register user
router.post('/api/register', (req, res) => {
    console.log("request =body ",req.body)
    const reg = new Register({
        name: req.body.name,
        email: req.body.email,
        mobile: req.body.mobile,
        password:req.body.password
    });
    reg.save((err, data) => {
        if(!err) {
            if(data){
                res.status(200).json({code: 200, message: 'User Added Successfully', addUser: data})
            }
            // res.send(data);
        } else {
            console.log(err);
            res.send(err);
        }
    });
});

// Get Single user

router.get('/api/user/:email/pass/:password', (req, res) => {
    console.log("================================================")
    console.log("================================================")
    console.log("================================================")
    console.log("request =======body ",req.params.email)
    console.log("================================================")
    console.log("================================================")
    console.log("================================================")


    Register.findOne({email:req.params.email,password:req.params.password}, (err, data) => {
        if(!err) {
            res.send(data);
        } else {
           console.log(err);
        }
    });
});

// Get All categories
router.get('/api/categories', (req, res) => {
    Categories.find({}, (err, data) => {
        if(!err) {
            res.send(data);
        } else {
            console.log(err);
        }
    });
});

// Get All packages
router.get('/api/packages/:id', (req, res) => {
    console.log("================================================")
    console.log("================================================")
    console.log("================================================")
    console.log("request =======body ",req.params.id)
    console.log("================================================")
    console.log("================================================")
    console.log("================================================")
    Package.find({categoryId:req.params.id}, (err, data) => {
        if(!err) {
            res.send(data);
        } else {
            console.log(err);
        }
    });
});





module.exports = router;